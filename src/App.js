import React from 'react';
import rocket from './rocket.svg';
import './App.css';

import Calculator from './Calculator'



function App() {

  return (
    <div className="App">
      <header className="App-header">
        <img src={rocket} className="App-logo" alt="Rocket" />
        <p>
          Solid Rocket Motor Performance Calculator
        </p>
        <p style={{fontSize:'10pt'}}>
          Only works for Type A grain.
        </p>

      </header>
      <Calculator/>
    </div>
  );
}

export default App;
