//This program takes in student number, calculates requested values and outputs tables & plots.
//Currently only works for propellant grain A
//Interactive version available //

import React from 'react';
import {VictoryChart, VictoryTheme, VictoryLine, VictoryAxis, VictoryLegend} from 'victory';

class Calculator extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      //values in mm
      shapeOptions: {
        a: {
          type: "A",
          dm: 40,
          dp: 100,
          lp: 1200
        },
        b: {
          type: "B",
          dm: 40,
          dp: 120,
          lp: 1500,
          lk: 200,
          alpha:5
        }
      },
      compositionOptions: {
        a: {
          //Kelvin
          combustionTemp: 3325.34,
          //kg/m^3
          propellantDensity: 1750,
          //J(KgK)
          gasConstant: 292.172,
          //k
          specificHeatRatio: 1.2,
          //for burning rate in m/s pressue Pa
          referenceLinearBurnRateCoeff: 0.0000086,
          burningRateExponentialCoeff: 0.49,
          //1/K
          tempSensitivityCoeff: 0.0025
        },
        b:{
          //Kelvin
          combustionTemp: 2630.56,
          //kg/m^3
          propellantDensity: 1610,
          //J(KgK)
          gasConstant: 329.616,
          //k
          specificHeatRatio: 1.232,
          //for burning rate in m/s pressue Pa
          referenceLinearBurnRateCoeff: 0.0000225,
          burningRateExponentialCoeff: 0.412,
          //1/K
          tempSensitivityCoeff: 0.0025
        }
      },
      //mm
      throatDiameterOptions : {
        a: 50 * 0.001,
        b: 52 * 0.001
      },
      expansionRatioOptions: {
        a: 3.5,
        b: 4,
      },

      shapeType: null,
      composition: null,
      expansionRatio: null,
      throatDiameter: null,
      //Degrees C
      propellantTemperatures: [-40, 15,60],
      //Degress
      nozzleDivergence: 15 ,
      numberOfNozzles: 1,
      //Pa
      ambientPressure: 101325,
      //Degrees C
      referenceTemperature: 15,
      burningSurfaceAgainstWeb: null,
      testE : [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64],
      testSb: [22771.53308,22956.01713, 24340.50103,26735.88402,29150.36733, 31148.64121,32301.86021,33864.49879, 35687.27621,36985.47949, 38164.55313, 39570.19006, 40893.57124,41635.63363,42127.39035, 42403.98444, 42540.04149, 42540.03831,42540.03782, 42540.02752,42540.02722, 42540.03015, 42031.55136,40280.10079, 37279.72389, 33522.49493, 29098.21331, 23316.39674, 17959.97854, 11090.29488, 4173.713238,511.897096,0],
      testBurnRates: [13.20671358, 13.28734866,13.88751718, 14.90655132,15.9112384, 16.72732838, 17.19242548, 17.81619581, 18.53494931, 19.04135508,19.49751773,20.03683789, 20.5403114,20.82087049,21.00611711,21.11007761,21.16115479, 21.16115359,21.16115341,21.16114955, 21.16114943,21.16115053,20.97005605,20.30742528, 19.15552331, 17.68029079,15.88975827, 13.4444077,11.04149253, 7.67516085, 3.672067257,0.754064623,0],
      burningSurface: [0],
      burntWeb: [0],
      time:[[0],[0],[0]],
      pressure:[0],
      pressureBar: [[0],[0],[0]],
      burnRates: [[0],[0],[0]],
      thrust: [[0],[0],[0]],
      pressureIntegral:[[0],[0],[0]],
      totalImpulse: [[0],[0],[0]],
      throatArea: null
    };


    this.evaluateConstants = this.evaluateConstants.bind(this);
  }

  isEven(value) {
  	if (value%2 === 0)
  		return true;
  	else
  		return false;
  }

  evaluateConstants (studentNumber) {
  let digitsAsString = studentNumber.split('');
  let digits = digitsAsString.map(Number);
  console.log(digits);
  let shape;
  let composition;
  let expansionRatio;
  let throatArea;


  (this.isEven(digits[2])) ? shape = this.state.shapeOptions.a : shape = this.state.shapeOptions.b;
  (this.isEven(digits[3])) ? composition = this.state.compositionOptions.a : composition = this.state.compositionOptions.b;
  (this.isEven(digits[4])) ? expansionRatio = this.state.expansionRatioOptions.a : expansionRatio = this.state.expansionRatioOptions.b;
  (this.isEven(digits[5])) ? throatArea = Math.PI * Math.pow((this.state.throatDiameterOptions.a/2),2) : throatArea = Math.PI * Math.pow((this.state.throatDiameterOptions.b/2),2);

  console.log(shape);
  this.setState({
    shape:shape,
    composition:composition,
    expansionRatio: expansionRatio,
    throatArea:throatArea
  },
  () => {this.calculateBurningSurface(shape);  this.setState({propellantMass: this.calculatePropellantMass(this.state.shape,this.state.composition.propellantDensity)})}

)


  }

  calculateThroatArea(throatDiameter){
    return Math.PI * Math.pow((this.state.throatDiameter/2),2)
  }

  calculateLinearBurnRateCoeff(tempSensitivityCoeff,referenceLinearBurnRateCoeff, referenceTemperature, propellantTemperature){
    return Math.exp((tempSensitivityCoeff * (propellantTemperature - referenceTemperature)) + Math.log(referenceLinearBurnRateCoeff))
  }

  //expects shape values in mm and density in kg/m^3 as provided.
  calculatePropellantMass(shape, density){
    console.log(shape);

    if(shape.type === "A") {
      //m^2
      let propellantSurfaceArea = ((Math.PI * Math.pow((shape.dp / 2),2)) - (Math.PI * Math.pow((shape.dm / 2),2)))/ 1e+6;
      //m^3
      let propellantVolume = propellantSurfaceArea * (shape.lp * 0.001);
      console.log(propellantSurfaceArea + " m^2");
      console.log(propellantVolume + " m^3");
      //kg
      let propellantMass = propellantVolume * density;
      console.log(propellantMass + " kg");
      return propellantMass;
    }

    if(shape.type === "B") {
        console.log(shape);
      //Part a, straight Part
        let part1Length = shape.lp - shape.lk;
        let part1XSection = ((Math.PI * Math.pow((shape.dp / 2),2)) - (Math.PI * Math.pow((shape.dm / 2),2)))/ 1e+6;
        let part1Volume = part1XSection * (part1Length * 0.001)
        console.log(`part1 volume: ${part1Volume} m^2`);
      //Part b, conical Part
        let part2Length = shape.lk;
        let part2TotalCylindricalVolume = (Math.PI *  Math.pow((shape.dp / 2),2)) * part2Length ;
        console.log(`part 2 cylindrical volume: ${part2TotalCylindricalVolume} mm^3`);
        let part2R1 = shape.dm / 2;
        let part2R2 = (Math.tan(shape.alpha * (Math.PI/180)) * shape.lk) + part2R1;

        let part2ConicalVolume = 1/3 * Math.PI * (Math.pow(part2R1,2) + (part2R1 * part2R2) + (Math.pow(part2R2),2)) * shape.lk;
        console.log(`part 2 conical volume: ${part2ConicalVolume} mm^3`);
        let part2Volume = (part2TotalCylindricalVolume - part2ConicalVolume) / 1e+9;
        console.log(`Part 2 Total Volume ${part2Volume} m^3`);

        console.log((part2Volume/part1Volume) * 100)
        let totalVolume = part1Volume + part2Volume;
        console.log(`Total Volume ${totalVolume} m^3`);
        let propellantMass = totalVolume * density;
        console.log(`Propellant Mass ${propellantMass} kg`);
      return propellantMass;
    }


  }

  calculateCharacteristicVelocity(k,combustionTemp,gasConstant) {
    let x =Math.sqrt(k)*Math.pow(2/(k + 1),((k + 1)/(2*(k-1))))
    return (Math.sqrt(gasConstant * combustionTemp)/x)
  }

  calculateBurningSurface (shape){
    if(shape.type === "A") {
      let totalWeb = (shape.dp - shape.dm) /4
      let burntWeb = Array.apply(null, {length: totalWeb +1}).map(Number.call, Number)
      burntWeb.push(15);
      this.setState({burntWeb:burntWeb})
      let burningSurface = [];
      for (let i=0; burntWeb[i] >= 0; i++){
        let y = (shape.dp + shape.dm) * Math.PI * (shape.lp + (2 * totalWeb) - 4 * burntWeb[i])
        burningSurface.push(y);
      }
      burningSurface[burningSurface.length - 1] = 0;
      this.setState({burningSurface:burningSurface})
      this.calculatePressure(burntWeb,burningSurface,this.state.composition.propellantDensity,this.state.propellantTemperatures,this.calculateCharacteristicVelocity(this.state.composition.specificHeatRatio,this.state.composition.combustionTemp, this.state.composition.gasConstant),this.state.throatArea,this.state.composition.burningRateExponentialCoeff,this.state.composition.tempSensitivityCoeff, this.state.composition.referenceLinearBurnRateCoeff, this.state.referenceTemperature)
    } else {
      alert("Type B not supported");
    }

  }

  calculatePressure(burntWeb, burningSurface,propellantDensity,propellantTemperatures,charactaristicVelocity,throatArea,burningRateExponentialCoeff,tempSensitivityCoeff, referenceLinearBurnRateCoeff, referenceTemperature){
    console.log(arguments);
    let burningSurfaceInMeteres = burningSurface.map(function(x) {return x * 1e-6})
    let pressureArray = [[],[],[]];
    let pressureArrayBar = [[],[],[]];

    for (let x=0; x < propellantTemperatures.length; x++){
      let linearBurnRateCoeff = this.calculateLinearBurnRateCoeff(tempSensitivityCoeff,referenceLinearBurnRateCoeff,referenceTemperature,propellantTemperatures[x])
      for (let i=0; i < burntWeb.length; i++){
        let pressure = Math.pow(propellantDensity * linearBurnRateCoeff * charactaristicVelocity * (burningSurfaceInMeteres[i])/throatArea,(1/(1-burningRateExponentialCoeff)))
        let pressureInBar = pressure * 1e-5
        pressureArray[x][i] = (pressure);
        pressureArrayBar[x][i] = pressureInBar
      }
      this.calculateBurnRates(linearBurnRateCoeff,pressureArray[x],burningRateExponentialCoeff,burntWeb,tempSensitivityCoeff,referenceTemperature,propellantTemperatures[x])
      this.calculateThrust(burntWeb,pressureArray[x]);
      this.calculatePressureIntegral(burntWeb, pressureArray[x]);
    }

    console.log(pressureArray);
    this.setState({pressure: pressureArray.reverse(), pressureBar: pressureArrayBar.reverse()});

    return pressureArray;
  }

  calculateBurnRates(burningRateLinearCoeff, pressures,burningRateExponentialCoeff,burntWeb,tempSensitivityCoeff, referenceTemperature, propellantTemperature){
    let burnRateArray = []
      for(let i=0; i< burntWeb.length; i++){
        let burnRate = burningRateLinearCoeff * Math.pow(pressures[i],burningRateExponentialCoeff) * Math.exp(tempSensitivityCoeff * (propellantTemperature - referenceTemperature))
        //Push to burn rate array and convert to mm/s
        burnRateArray.push(burnRate * 1000);

      }
    this.calculateTimeBetweenBurntWebPoints(burntWeb, burnRateArray)
    this.setState(prevState => ({
      burnRates: [burnRateArray, ...prevState.burnRates]
    }));

    return burnRateArray;

  }

  calculateTimeBetweenBurntWebPoints(burntWeb,burnRates){
    let timeBetweenBurntWebPointsArray = []
    //let testTopline = 2 * ()
    for(let i=1; i < burntWeb.length; i++){
      let timeBetweenBurntWebPoints = ((2 ) * (burntWeb[i] - burntWeb[i-1]))/((burnRates[i] + burnRates[i-1]))
      timeBetweenBurntWebPointsArray.push(timeBetweenBurntWebPoints);
    }
    this.calculateBurnTime(burntWeb,timeBetweenBurntWebPointsArray)
    return timeBetweenBurntWebPointsArray
  }

  calculateBurnTime(burntWeb,timeBetweenBurntWebPointsArray){
    let burnTimes = [0]
    for(let i =0; i < (burntWeb.length -1); i++){
      burnTimes.push(burnTimes[i] + timeBetweenBurntWebPointsArray[i])
    }
    this.setState(prevState => ({
      time: [burnTimes, ...prevState.time]
    }));
  }

  calculateExitMachNumber(specificHeatRatio,expansionRatio){

    var f1 = (specificHeatRatio+1)/(2.0*(specificHeatRatio-1));
     var a0 = 2.0;
     var m0 = 2.2;
     var m1=0;
     var fac=0;
     var a1=0;
     var am = 0;

     m1 = m0 + 0.05;
     while(Math.abs(expansionRatio-a0) > 0.0001){
       fac = 1.0 + 0.5*(specificHeatRatio-1)*m1*m1;
       a1 = 1.0/(m1 * Math.pow(fac, -1*f1) * Math.pow((specificHeatRatio+1)/2.0, f1));
       am = (a1-a0)/(m1-m0);
       a0 = a1;
       m0 = m1;
       m1 = m0 + ((expansionRatio - a0)/am);
     }
     let mach = m0;
     return mach;

  }

  calculateExitPressureRatio(specificHeatRatio,exitMachNumber){
    return Math.pow(1 + (specificHeatRatio-1)/2 * Math.pow(exitMachNumber,2),-1 * ((specificHeatRatio)/(specificHeatRatio -1)))

  }

  calculateThrustCoefficient(specificHeatRatio,exitPressureRatio,atmosphericPressure, initialChamberPressure){
    //k = specificHeatRatio for brevity
    console.log(arguments)
    let k = specificHeatRatio;
    let pAtmosAgainstP0 = atmosphericPressure / initialChamberPressure

    return (Math.sqrt(k) * Math.pow(2/(k + 1),(k+1)/(2*(k -1)))) * Math.sqrt( ((2*k)/(k -1)) * (1 - Math.pow(exitPressureRatio,((k-1)/k)))) + ((Math.pow(2/(k+1), 1/(k-1)) * Math.sqrt((k -1)/(k +1))) / (Math.pow(exitPressureRatio,1/k) * Math.sqrt((1 - (Math.pow(exitPressureRatio,(k-1)/k)))))) * (exitPressureRatio - pAtmosAgainstP0)
  }

  calculateThrustCorrectionFactor(){
    let alMassFraction = 0.21 //from lectures
    let expansionRatio = this.state.expansionRatio;
    let nozzleDivergence = this.state.nozzleDivergence;
    console.log(expansionRatio);
    console.log(nozzleDivergence);
    return 1.0605 + (0.0111 * Math.log(this.state.throatArea/25.4)) - 0.0328 * Math.log(nozzleDivergence) - (0.254 * (alMassFraction)) - (0.00617 * Math.log(expansionRatio))

  }

calculateThrust(burntWeb, pressure){
  let shr = this.state.composition.specificHeatRatio;
  let expRatio = this.state.expansionRatio;
  let pAtmos = this.state.ambientPressure;
  let exitMachNumber = this.calculateExitMachNumber(shr,expRatio);
  console.log(`exit mach number: ${exitMachNumber}`);
  let exitPressureRatio = this.calculateExitPressureRatio(shr,exitMachNumber)
  console.log(`exitPressureRatio: ${exitPressureRatio}`);
  let idealThrustCoeff = this.calculateThrustCoefficient(shr,exitPressureRatio,pAtmos,pressure[0]);
  console.log(`idealThrustCoeff: ${idealThrustCoeff}`);
  let correctionFactor = this.calculateThrustCorrectionFactor();
  console.log(`correctionFactor: ${correctionFactor}`);
  let realThrustCoeff = idealThrustCoeff * correctionFactor;
  console.log(`realThrustCoeff: ${realThrustCoeff}`);
  let thrust = []
  for (let i=0; i< pressure.length; i++ ){
    thrust.push(realThrustCoeff * pressure[i] * this.state.throatArea);

  }
  this.setState(prevState => ({
    thrust: [thrust, ...prevState.thrust]
  }));
    console.log(thrust);
    this.calculateTotalImpulse(burntWeb, thrust);

}

calculatePressureIntegral(burntWeb, pressure){
  console.log(pressure[0]);
  let h = (burntWeb[burntWeb.length - 1] - burntWeb[0])/(burntWeb.length - 1)
  let yx =0;
  for(let i =1; i < (burntWeb.length -2); i++){

    yx += pressure[i];
  }

  let pressureIntegral = (0.5 * h) * ((pressure[0]) + pressure[pressure.length-2]) + (2 * yx)
  console.log(pressureIntegral)
  this.setState(prevState => ({
    pressureIntegral: [pressureIntegral, ...prevState.pressureIntegral]
  }));
}

calculateTotalImpulse(burntWeb, thrust){
  console.log(burntWeb)
  let h = (burntWeb[burntWeb.length - 1] - burntWeb[0])/(burntWeb.length - 1)
  let yx =0;
  for(let i =1; i < (burntWeb.length -2); i++){
    yx += thrust[i];
  }
  let totalImpulse = (0.5 * h) * ((thrust[0]) + thrust[thrust.length -2]) + (2 * yx)
  console.log(totalImpulse)
  this.setState(prevState => ({
    totalImpulse: [totalImpulse, ...prevState.totalImpulse]
  }));
}


calculateOnClick () {
  this.setState({propellantMass: this.calculatePropellantMass(this.state.shape,this.state.composition.propellantDensity)})
}

precise(x) {
  return Number.parseFloat(x).toPrecision(4);
}

  render() {
    return (
      <div>
      <div style={{marginTop: '20px'}}>
      <input
            type="text"
            placeholder="Student Number"
            onChange={(e) => {this.setState({studentNumber: e.target.value})}}
         />
      <button onClick={() => this.evaluateConstants(this.state.studentNumber)}> Calculate </button>
      </div>
      {this.state.composition && (
        <div>
          <p> Combustion Temp: {this.state.composition.combustionTemp}K</p>
          <p> Propellant Density: {this.state.composition.propellantDensity}K</p>
          <p> Gas Constant: {this.state.composition.gasConstant}K(KgK)</p>
          <p> Specfic Heat Ratio: {this.state.composition.specificHeatRatio}</p>
          <p> Burn Rate Linear Coeff: {this.state.composition.referenceLinearBurnRateCoeff}</p>
          <p> Burn Rate Exponential: {this.state.composition.burningRateExponentialCoeff}</p>
          <p> Temperature Sensitivity: {this.state.composition.tempSensitivityCoeff} </p>
      </div>
      )}

      {this.state.propellantMass && (
        <div>
          <p style={{color:'red'}}> Propellant Mass: {this.precise(this.state.propellantMass)}Kg</p>
          <p style={{color:'red'}}> Throat Area: {this.precise(this.state.throatArea)}m^2</p>
          <p style={{color:'red'}}> Expansion Ratio: {this.precise(this.state.expansionRatio)}</p>
      </div>
      )}

      <div style={{height: '400px', margin:'50px', display: 'flex'}}>

      <table>
        <tr>
          <th>Web</th>
          <th>Burning Surface</th>
        </tr>
        {this.state.burntWeb.map((row,i) => (
          <tr>
            <td>{this.state.burntWeb[i]} </td>
            <td>{this.precise(this.state.burningSurface[i])} </td>
          </tr>
        ))}
      </table>

            <VictoryChart
              theme={VictoryTheme.material}
              domainPadding={15}
              domain={{y:[0,550000]}}
            >
              <VictoryAxis
                  label="Burnt Web (mm)"
                  style={{
                    axisLabel: { padding: 30 }
                  }}
                />
              <VictoryAxis dependentAxis
                label="Burning Surface (mm^2)"
                style={{
                  axisLabel: { padding: 80 }
                }}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#DD7373" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.burningSurface.map((v,i) =>{
                  return {y:v,x:this.state.burntWeb[i]};
                })}

              />
          </VictoryChart>
      </div>
      <div style={{height: '400px', margin:'50px', display:'flex'}}>
      <table>
        <tr>
          <th>Burn Rates (mm/s)</th>
          <th>Time (s)</th>
          <th>Burn Rates (mm/s)</th>
          <th>Time (s)</th>
          <th>Burn Rates (mm/s)</th>
          <th>Time (s)</th>

        </tr>
        {this.state.burntWeb.map((row,i) => (
          <tr>
            <td>{this.precise(this.state.burnRates[0][i])} </td>
            <td>{this.precise(this.state.time[0][i])} </td>
            <td>{this.precise(this.state.burnRates[1][i])} </td>
            <td>{this.precise(this.state.time[1][i])} </td>
            <td>{this.precise(this.state.burnRates[2][i])} </td>
            <td>{this.precise(this.state.time[2][i])} </td>
          </tr>
        ))}
      </table>
            <VictoryChart
              theme={VictoryTheme.material}
              domainPadding={15}

            >
            <VictoryLegend x={350} y={50}
               orientation="vertical"
               gutter={20}
               style={{ border: { stroke: "black" }, title: {fontSize: 20 } }}
               data={[
                 { name: "60", symbol: { fill: "#DD7373" } },
                 { name: "15", symbol: { fill: "#64B6AC" } },
                 { name: "-40", symbol: { fill: "#3B3561" } }
               ]}
              />
              <VictoryAxis
                  label="Time(s)"
                  style={{
                    axisLabel: { padding: 30 }
                  }}
                />
              <VictoryAxis dependentAxis
                label="Burn Rate (mm/s)"
                style={{
                  axisLabel: { padding: 80 }
                }}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#DD7373" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.burnRates[0].map((v,i) =>{
                  return {y:v,x:this.state.time[0][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#64B6AC" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.burnRates[1].map((v,i) =>{
                  return {y:v,x:this.state.time[1][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#3B3561" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.burnRates[2].map((v,i) =>{
                  return {y:v,x:this.state.time[2][i]};
                })}
              />
          </VictoryChart>
      </div>
      <div style={{height: '400px', margin:'50px', display:'flex'}}>
      <table>
        <tr>
          <th>Pressure (Bar)</th>
          <th>Time (s)</th>
          <th>Pressure (Bar)</th>
          <th>Time (s)</th>
          <th>Pressure (Bar)</th>
          <th>Time (s)</th>

        </tr>
        {this.state.burntWeb.map((row,i) => (
          <tr>
            <td>{this.precise(this.state.pressureBar[0][i])} </td>
            <td>{this.precise(this.state.time[0][i])} </td>
            <td>{this.precise(this.state.pressureBar[1][i])} </td>
            <td>{this.precise(this.state.time[1][i])} </td>
            <td>{this.precise(this.state.pressureBar[2][i])} </td>
            <td>{this.precise(this.state.time[2][i])} </td>
          </tr>
        ))}
      </table>
            <VictoryChart
              theme={VictoryTheme.material}
              domainPadding={15}

            >
            <VictoryLegend x={350} y={50}
               orientation="vertical"
               gutter={20}
               style={{ border: { stroke: "black" }, title: {fontSize: 20 } }}
               data={[
                 { name: "60", symbol: { fill: "#DD7373" } },
                 { name: "15", symbol: { fill: "#64B6AC" } },
                 { name: "-40", symbol: { fill: "#3B3561" } }
               ]}
              />
              <VictoryAxis
                  label="Time(s)"
                  style={{
                    axisLabel: { padding: 30 }
                  }}
                />
              <VictoryAxis dependentAxis
                label="Pressure (Bar)"
                style={{
                  axisLabel: { padding: 80 }
                }}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#DD7373" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.pressureBar[0].map((v,i) =>{
                  return {y:v,x:this.state.time[0][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#64B6AC" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.pressureBar[1].map((v,i) =>{
                  return {y:v,x:this.state.time[1][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#3B3561" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.pressureBar[2].map((v,i) =>{
                  return {y:v,x:this.state.time[2][i]};
                })}
              />
          </VictoryChart>
      </div>
      <div style={{height: '400px', margin:'50px', display:'flex'}}>
      <table>
        <tr>
          <th>Thrust (N)</th>
          <th>Time (s)</th>
          <th>Thrust (N)</th>
          <th>Time (s)</th>
          <th>Thrust (N)</th>
          <th>Time (s)</th>

        </tr>
        {this.state.burntWeb.map((row,i) => (
          <tr>
            <td>{this.precise(this.state.thrust[0][i])} </td>
            <td>{this.precise(this.state.time[0][i])} </td>
            <td>{this.precise(this.state.thrust[1][i])} </td>
            <td>{this.precise(this.state.time[1][i])} </td>
            <td>{this.precise(this.state.thrust[2][i])} </td>
            <td>{this.precise(this.state.time[2][i])} </td>
          </tr>
        ))}
      </table>
            <VictoryChart
              theme={VictoryTheme.material}
              domainPadding={15}

            >
            <VictoryLegend x={350} y={50}
               orientation="vertical"
               gutter={20}
               style={{ border: { stroke: "black" }, title: {fontSize: 20 } }}
               data={[
                 { name: "60", symbol: { fill: "#DD7373" } },
                 { name: "15", symbol: { fill: "#64B6AC" } },
                 { name: "-40", symbol: { fill: "#3B3561" } }
               ]}
              />
              <VictoryAxis
                  label="Time(s)"
                  style={{
                    axisLabel: { padding: 30 }
                  }}
                />
              <VictoryAxis dependentAxis
                label="Thrust (N)"
                style={{
                  axisLabel: { padding: 80 }
                }}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#DD7373" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.thrust[0].map((v,i) =>{
                  return {y:v,x:this.state.time[0][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#64B6AC" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.thrust[1].map((v,i) =>{
                  return {y:v,x:this.state.time[1][i]};
                })}
              />
              <VictoryLine
                style={{
                  data: { stroke: "#3B3561" },
                  parent: { border: "1px solid #ccc"}
                }}
                data={this.state.thrust[2].map((v,i) =>{
                  return {y:v,x:this.state.time[2][i]};
                })}
              />
          </VictoryChart>
      </div>
      {this.state.propellantMass && (
        <div>
          <p style={{color:'red'}}> Pressure Integral (60): {this.precise(this.state.pressureIntegral[0])}</p>
          <p style={{color:'red'}}> Pressure Integral (15): {this.precise(this.state.pressureIntegral[1])}</p>
          <p style={{color:'red'}}> Pressure Integral (-40): {this.precise(this.state.pressureIntegral[2])}</p>
      </div>
      )}

      {this.state.propellantMass && (
        <div>
          <p style={{color:'red'}}> Total Impulse (60): {this.precise(this.state.totalImpulse[0])}Ns</p>
          <p style={{color:'red'}}> Total Impulse (15): {this.precise(this.state.totalImpulse[1])}Ns</p>
          <p style={{color:'red'}}> Total Impulse (-40): {this.precise(this.state.totalImpulse[2])}Ns</p>
      </div>
      )}

</div>
    );
  }
}
export default Calculator;
